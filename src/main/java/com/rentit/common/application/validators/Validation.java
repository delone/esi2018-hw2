package com.rentit.common.application.validators;

import com.rentit.common.application.exceptions.*;
import com.rentit.inventory.domain.model.PlantInventoryEntry;
import com.rentit.inventory.domain.model.PlantInventoryItem;
import com.rentit.sales.domain.model.PurchaseOrder;
import org.springframework.validation.DataBinder;
import org.springframework.validation.ObjectError;

import java.util.List;

public class Validation {

    private static DataBinder binder;

    public static void purchaseOrderValidator(PurchaseOrder po) throws PurchaseOrderException, PurchaseOrderInvalidDateException, PurchaseOrderNotFoundException, PlantInventoryEntryNotFoundException {

        purchaseOrderExists(po);
        plantInventoryEntryExists(po.getPlant());

        binder = new DataBinder(po);
        binder.addValidators(new PurchaseOrderValidator());
        binder.validate();

        if (binder.getBindingResult().hasErrors()){
            List<ObjectError> errorList = binder.getBindingResult().getAllErrors();
            throw new PurchaseOrderInvalidDateException(errorList.get(0).getCode());
        }
    }

    public static void plantInventoryItemValidator(PlantInventoryItem item) throws PlantInventoryItemNotFoundException, PlantInventoryItemException {

        plantInventoryItemExists(item);

        binder = new DataBinder(item);
        binder.addValidators(new PlantInventoryItemValidator());
        binder.validate();

        if (binder.getBindingResult().hasErrors()){
            List<ObjectError> errorList = binder.getBindingResult().getAllErrors();
            throw new PlantInventoryItemException(errorList.get(0).getCode());
        }
    }

    private static void purchaseOrderExists(PurchaseOrder po) throws PurchaseOrderNotFoundException {
        if (po == null)
            throw new PurchaseOrderNotFoundException();
    }

    private static void plantInventoryEntryExists(PlantInventoryEntry entry) throws PlantInventoryEntryNotFoundException {
        if (entry == null)
            throw new PlantInventoryEntryNotFoundException();
    }

    private static void plantInventoryItemExists(PlantInventoryItem item) throws PlantInventoryItemNotFoundException {
        if (item == null)
            throw new PlantInventoryItemNotFoundException();
    }

}
