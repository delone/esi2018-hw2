package com.rentit.inventory.application.services;

import com.rentit.inventory.application.dto.PlantInventoryItemDTO;
import com.rentit.inventory.domain.model.PlantInventoryItem;
import com.rentit.inventory.rest.InventoryRestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PlantInventoryItemAssembler extends ResourceAssemblerSupport<PlantInventoryItem, PlantInventoryItemDTO> {

    @Autowired
    PlantInventoryEntryAssembler plantInventoryEntryAssembler;

    public PlantInventoryItemAssembler() {
        super(InventoryRestController.class, PlantInventoryItemDTO.class);
    }

    @Override
    public PlantInventoryItemDTO toResource(PlantInventoryItem plantInventoryItem) {
        PlantInventoryItemDTO dto = createResourceWithId(plantInventoryItem.getId(), plantInventoryItem);
        dto.set_id(plantInventoryItem.getId());
        dto.setEquipmentCondition(plantInventoryItem.getEquipmentCondition());
        dto.setPlantInfo(plantInventoryEntryAssembler.toResource(plantInventoryItem.getPlantInfo()));
        dto.setSerialNumber(plantInventoryItem.getSerialNumber());

        return dto;
    }

    public List<PlantInventoryItemDTO> toResources(List<PlantInventoryItem> plantInventoryItems) {

        List<PlantInventoryItemDTO> dtoList = new ArrayList<>();
        plantInventoryItems.forEach(po -> dtoList.add(toResource(po)));
        return dtoList;
    }

}
