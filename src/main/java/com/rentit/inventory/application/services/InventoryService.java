package com.rentit.inventory.application.services;

import com.rentit.common.application.exceptions.PlantInventoryEntryNotFoundException;
import com.rentit.common.application.exceptions.PurchaseOrderException;
import com.rentit.common.application.validators.Validation;
import com.rentit.inventory.application.dto.PlantInventoryEntryDTO;
import com.rentit.inventory.application.dto.PlantInventoryItemDTO;
import com.rentit.inventory.domain.model.PlantInventoryEntry;
import com.rentit.inventory.domain.model.PlantInventoryItem;
import com.rentit.inventory.domain.repository.InventoryRepository;
import com.rentit.sales.domain.model.PurchaseOrder;
import com.rentit.sales.domain.repository.PurchaseOrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class InventoryService {

    @Autowired
    InventoryRepository inventoryRepository;

    @Autowired
    PlantInventoryEntryAssembler plantInventoryEntryAssembler;

    @Autowired
    PlantInventoryItemAssembler plantInventoryItemAssembler;

    @Autowired
    PurchaseOrderRepository purchaseOrderRepository;

    public List<PlantInventoryEntryDTO> findAvailable(String plantName, LocalDate startDate, LocalDate endDate) {
        List<PlantInventoryEntry> res = inventoryRepository.findAvailablePlants(plantName,startDate, endDate);
        return plantInventoryEntryAssembler.toResources(res);
    }

    public List<PlantInventoryItemDTO> findAvailableItems(Long id) throws PurchaseOrderException, PlantInventoryEntryNotFoundException {

        PurchaseOrder po = purchaseOrderRepository.findOne(id);
        Validation.purchaseOrderValidator(po);


        PlantInventoryEntry entry = po.getPlant();
        LocalDate startDate = po.getRentalPeriod().getStartDate();
        LocalDate endDate = po.getRentalPeriod().getEndDate();

        List<PlantInventoryItem> res = inventoryRepository.findAvailablePlantItems(entry, startDate, endDate);
        return plantInventoryItemAssembler.toResources(res);
    }

}
