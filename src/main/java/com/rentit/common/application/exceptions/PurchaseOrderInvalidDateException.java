package com.rentit.common.application.exceptions;

public class PurchaseOrderInvalidDateException extends PurchaseOrderException {
    public PurchaseOrderInvalidDateException(String message) {
        super(PurchaseOrderInvalidDateException.class.getName() + " : " + message);
    }
}
