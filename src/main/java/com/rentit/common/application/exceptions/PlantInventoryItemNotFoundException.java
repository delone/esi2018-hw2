package com.rentit.common.application.exceptions;

public class PlantInventoryItemNotFoundException extends Exception {
    public PlantInventoryItemNotFoundException() {
        super(PlantInventoryItemException.class.getName() + " : " + "Plant Inventory Item not found!");
    }
}
