package com.rentit.sales.rest;

import com.rentit.common.application.exceptions.*;
import com.rentit.inventory.application.dto.PlantInventoryEntryDTO;
import com.rentit.inventory.application.dto.PlantInventoryItemDTO;
import com.rentit.inventory.application.services.InventoryService;
import com.rentit.sales.application.dto.PurchaseOrderDTO;
import com.rentit.sales.application.services.SalesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("api/sales")
public class SalesRestController {

    @Autowired
    InventoryService inventoryService;

    @Autowired
    SalesService salesService;

    @GetMapping("/plants")
    public List<PlantInventoryEntryDTO> findAvailablePlants(
            @RequestParam(name = "name") String plantName,
            @RequestParam(name = "startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
            @RequestParam(name = "endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate) {
        return inventoryService.findAvailable(plantName, startDate, endDate);
    }

    @GetMapping("/orders/{id}")
    @ResponseStatus(HttpStatus.OK)
    public PurchaseOrderDTO fetchPurchaseOrder(@PathVariable("id") Long id) {
        return salesService.findPurchaseOrder(id);
    }

    @GetMapping("/orders")
    @ResponseStatus(HttpStatus.OK)
    public List<PurchaseOrderDTO> purchaseOrderWithPendingStatus(){
        return salesService.findPendingPurchaseOrders();
    }

    @GetMapping("/orders/{oid}/plant/items")
    @ResponseStatus(HttpStatus.OK)
    public List<PlantInventoryItemDTO> findPlantInventoryItems(@PathVariable("oid") Long oid) throws PurchaseOrderException, PlantInventoryEntryNotFoundException {
        return inventoryService.findAvailableItems(oid);
    }

    @PostMapping("/orders")
    public ResponseEntity<PurchaseOrderDTO> createPurchaseOrder(@RequestBody PurchaseOrderDTO partialPODTO) throws URISyntaxException, PlantNotFoundException, PurchaseOrderException, PlantInventoryEntryNotFoundException {
        PurchaseOrderDTO newlyCreatePODTO = salesService.createPurchaseOrder(partialPODTO);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(new URI(newlyCreatePODTO.getId().getHref()));

        return new ResponseEntity<>(newlyCreatePODTO, headers, HttpStatus.CREATED);
    }

    @PostMapping("/orders/{oid}/accept")
    public ResponseEntity<PurchaseOrderDTO> accept(@PathVariable("oid") Long oid, @RequestBody PlantInventoryItemDTO item)
            throws URISyntaxException, PurchaseOrderException, PlantInventoryEntryNotFoundException, PlantInventoryItemNotFoundException, PlantInventoryItemException {
        PurchaseOrderDTO poDTO = salesService.allocatePlantWithPurchaseOrder(oid, item);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(new URI(poDTO.getId().getHref()));

        return new ResponseEntity<>(poDTO, headers, HttpStatus.CREATED);
    }

    @DeleteMapping("/orders/{oid}/accept")
    public ResponseEntity<PurchaseOrderDTO> reject(@PathVariable("oid") Long oid) throws URISyntaxException, PurchaseOrderException, PlantInventoryEntryNotFoundException {
        PurchaseOrderDTO poDTO = salesService.rejectPurchaseOrder(oid);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(new URI(poDTO.getId().getHref()));

        return new ResponseEntity<>(poDTO, headers, HttpStatus.OK);
    }

    @PostMapping("/orders/{oid/close")
    public ResponseEntity<PurchaseOrderDTO> close(@PathVariable("oid") Long oid) throws PurchaseOrderException, URISyntaxException, PlantInventoryEntryNotFoundException {
        PurchaseOrderDTO poDTO = salesService.closePurchaseOrder(oid);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(new URI(poDTO.getId().getHref()));

        return new ResponseEntity<>(poDTO, headers, HttpStatus.OK);
    }

}
