package com.rentit.common.application.exceptions;

public class PlantInventoryEntryNotFoundException extends PlantInventoryEntryException {
    public PlantInventoryEntryNotFoundException() {
        super(PlantInventoryEntryNotFoundException.class.getName() + " : " + "Plant Entry not found!");
    }
}
