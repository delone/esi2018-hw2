package com.rentit.sales.domain.model;

import com.rentit.common.domain.model.BusinessPeriod;
import com.rentit.inventory.domain.model.PlantInventoryEntry;
import com.rentit.inventory.domain.model.PlantReservation;
import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;

@Entity
@Getter
@NoArgsConstructor(force = true, access = AccessLevel.PROTECTED)
public class PurchaseOrder {
    @Id @GeneratedValue
    Long id;

    public static PurchaseOrder of(PlantInventoryEntry plant, BusinessPeriod rentalPeriod) {
        PurchaseOrder po = new PurchaseOrder();
        po.plant = plant;
        po.rentalPeriod = rentalPeriod;
        po.status = POStatus.PENDING;
        po.total = (plant != null) ? calculateTotalPrice(plant, rentalPeriod) : null;
        return po;
    }

    @OneToMany
    List<PlantReservation> reservations;
    @ManyToOne
    PlantInventoryEntry plant;

    LocalDate issueDate;
    LocalDate paymentSchedule;

    @Column(precision = 8, scale = 2)
    BigDecimal total;

    @Enumerated(EnumType.STRING)
    POStatus status;

    @Embedded
    BusinessPeriod rentalPeriod;

    @Override
    public String toString() {
        return "PurchaseOrder{" +
                "id=" + id +
                ", reservations=" + reservations +
                ", plant=" + plant +
                ", issueDate=" + issueDate +
                ", paymentSchedule=" + paymentSchedule +
                ", total=" + total +
                ", status=" + status +
                ", rentalPeriod=" + rentalPeriod +
                '}';
    }

    public void registerAllocation(PlantReservation reservation) {
        reservations.add(reservation);
        status = POStatus.OPEN;
        rentalPeriod = BusinessPeriod.of(reservation.getSchedule().getStartDate(), reservation.getSchedule().getEndDate());
        // UPDATE PO total!!
    }

    public void changePurchaseOrderStatus(POStatus status){
        this.status = status;
    }

    private static BigDecimal calculateTotalPrice(PlantInventoryEntry entry, BusinessPeriod period){
        LocalDate startDate = period.getStartDate();
        LocalDate endDate = period.getEndDate();
        BigDecimal price = entry.getPrice();

        BigDecimal days = new BigDecimal(ChronoUnit.DAYS.between(startDate, endDate));

        return days.multiply(price);
    }
}
