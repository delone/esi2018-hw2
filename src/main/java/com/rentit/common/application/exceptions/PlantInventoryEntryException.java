package com.rentit.common.application.exceptions;

public class PlantInventoryEntryException extends Exception {
    public PlantInventoryEntryException(String message) {
        super(message);
    }
}
