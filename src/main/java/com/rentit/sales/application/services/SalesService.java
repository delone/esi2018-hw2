package com.rentit.sales.application.services;

import com.rentit.common.application.exceptions.*;
import com.rentit.common.application.validators.Validation;
import com.rentit.common.domain.model.BusinessPeriod;
import com.rentit.inventory.application.dto.PlantInventoryItemDTO;
import com.rentit.inventory.application.services.PlantInventoryEntryAssembler;
import com.rentit.inventory.application.services.PlantReservationAssembler;
import com.rentit.inventory.domain.model.PlantInventoryEntry;
import com.rentit.inventory.domain.model.PlantInventoryItem;
import com.rentit.inventory.domain.model.PlantReservation;
import com.rentit.inventory.domain.repository.PlantInventoryEntryRepository;
import com.rentit.inventory.domain.repository.PlantInventoryItemRepository;
import com.rentit.inventory.domain.repository.PlantReservationRepository;
import com.rentit.sales.application.dto.PurchaseOrderDTO;
import com.rentit.sales.domain.model.POStatus;
import com.rentit.sales.domain.model.PurchaseOrder;
import com.rentit.sales.domain.repository.PurchaseOrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SalesService {

    PlantInventoryEntryRepository plantInventoryEntryRepository;
    PurchaseOrderRepository purchaseOrderRepository;
    PlantReservationRepository plantReservationRepository;
    PlantInventoryItemRepository plantInventoryItemRepository;
    PurchaseOrderAssembler purchaseOrderAssembler;
    PlantReservationAssembler plantReservationAssembler;
    PlantInventoryEntryAssembler plantInventoryEntryAssembler;

    @Autowired
    public SalesService(PlantInventoryEntryRepository plantInventoryEntryRepository, PurchaseOrderRepository purchaseOrderRepository, PlantReservationRepository plantReservationRepository, PlantInventoryItemRepository plantInventoryItemRepository, PurchaseOrderAssembler purchaseOrderAssembler, PlantReservationAssembler plantReservationAssembler, PlantInventoryEntryAssembler plantInventoryEntryAssembler) {
        this.plantInventoryEntryRepository = plantInventoryEntryRepository;
        this.purchaseOrderRepository = purchaseOrderRepository;
        this.plantReservationRepository = plantReservationRepository;
        this.plantInventoryItemRepository = plantInventoryItemRepository;
        this.purchaseOrderAssembler = purchaseOrderAssembler;
        this.plantReservationAssembler = plantReservationAssembler;
        this.plantInventoryEntryAssembler = plantInventoryEntryAssembler;
    }

   public PurchaseOrderDTO findPurchaseOrder(Long id) {
       PurchaseOrder po = purchaseOrderRepository.findOne(id);
       return purchaseOrderAssembler.toResource(po);
   }

    public PurchaseOrderDTO createPurchaseOrder(PurchaseOrderDTO partialPODTO) throws PlantNotFoundException, PlantInventoryEntryNotFoundException, PurchaseOrderException {
        PlantInventoryEntry plant = plantInventoryEntryRepository.findOne(partialPODTO.getPlant().get_id());
        BusinessPeriod rentalPeriod = BusinessPeriod.of(partialPODTO.getRentalPeriod().getStartDate(), partialPODTO.getRentalPeriod().getEndDate());

        PurchaseOrder po = PurchaseOrder.of(
                plant,
                rentalPeriod);

        Validation.purchaseOrderValidator(po);

        po = purchaseOrderRepository.save(po);

        return purchaseOrderAssembler.toResource(po);
    }

    public List<PurchaseOrderDTO> findPendingPurchaseOrders(){
        List<PurchaseOrder> po = purchaseOrderRepository.findByStatus(POStatus.PENDING);
        return purchaseOrderAssembler.toResources(po);
    }

    public PurchaseOrderDTO allocatePlantWithPurchaseOrder(Long orderID, PlantInventoryItemDTO itemDTO) throws PurchaseOrderException, PlantInventoryEntryNotFoundException, PlantInventoryItemNotFoundException, PlantInventoryItemException {

        PurchaseOrder po = purchaseOrderRepository.findOne(orderID);

        Validation.purchaseOrderValidator(po);

        PlantInventoryItem item = plantInventoryItemRepository.findOne(itemDTO.get_id());
        Validation.plantInventoryItemValidator(item);

        PlantReservation reservation = new PlantReservation();
        reservation.setPlant(item);
        reservation.setSchedule(BusinessPeriod.of(po.getRentalPeriod().getStartDate(), po.getRentalPeriod().getEndDate()));
        reservation = plantReservationRepository.save(reservation);

        po.registerAllocation(reservation);
        po.changePurchaseOrderStatus(POStatus.OPEN);
        po = purchaseOrderRepository.save(po);

        return purchaseOrderAssembler.toResource(po);
    }

    public PurchaseOrderDTO rejectPurchaseOrder(Long orderID) throws PurchaseOrderException, PlantInventoryEntryNotFoundException {
        return updatePOStatus(orderID, POStatus.CLOSED);
    }

    public PurchaseOrderDTO closePurchaseOrder(Long orderID) throws PurchaseOrderException, PlantInventoryEntryNotFoundException {
        return updatePOStatus(orderID, POStatus.CLOSED);
    }


    private PurchaseOrderDTO updatePOStatus(Long id, POStatus status) throws PurchaseOrderException, PlantInventoryEntryNotFoundException {
        PurchaseOrder po = purchaseOrderRepository.findOne(id);
        Validation.purchaseOrderValidator(po);

        po.changePurchaseOrderStatus(status);
        po = purchaseOrderRepository.save(po);

        return purchaseOrderAssembler.toResource(po);
    }
}
