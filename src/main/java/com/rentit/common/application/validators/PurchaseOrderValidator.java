package com.rentit.common.application.validators;

import com.rentit.sales.domain.model.PurchaseOrder;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.time.LocalDate;

public class PurchaseOrderValidator implements Validator {

    @Override
    public boolean supports(Class<?> aClass) {
        return PurchaseOrder.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        PurchaseOrder po = (PurchaseOrder) o;

//        ValidationUtils.rejectIfEmpty(errors, "plant", "This is not valid entry..");

        LocalDate now = LocalDate.now();
        LocalDate startDate = po.getRentalPeriod().getStartDate();
        LocalDate endDate = po.getRentalPeriod().getEndDate();

        if (startDate.isAfter(endDate))
            errors.rejectValue("rentalPeriod", "End date cannot be before start date..");

        if (startDate.isBefore(now))
            errors.rejectValue("rentalPeriod", "Start date cannot be before today..");

    }

}
