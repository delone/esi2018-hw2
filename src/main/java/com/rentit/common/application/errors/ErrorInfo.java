package com.rentit.common.application.errors;

public class ErrorInfo {

    public final String url;
    public final String status;
    public final String message;

    public ErrorInfo(String url, String status, Exception message) {
        this.url = url;
        this.status = status;
        this.message = message.getLocalizedMessage();
    }
}
