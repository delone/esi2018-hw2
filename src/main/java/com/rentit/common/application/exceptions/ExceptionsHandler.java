package com.rentit.common.application.exceptions;

import com.rentit.common.application.errors.ErrorInfo;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class ExceptionsHandler {

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(PlantInventoryEntryNotFoundException.class)
    @ResponseBody
    ErrorInfo
    handPlantEntryNotFoundException(HttpServletRequest req, Exception ex) {
        return new ErrorInfo(req.getRequestURL().toString(),"" + HttpStatus.NOT_FOUND, ex);
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(PlantInventoryItemNotFoundException.class)
    @ResponseBody
    ErrorInfo
    handPlantItemNotFoundException(HttpServletRequest req, Exception ex) {
        return new ErrorInfo(req.getRequestURL().toString(),"" + HttpStatus.NOT_FOUND, ex);
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(PurchaseOrderNotFoundException.class)
    @ResponseBody
    ErrorInfo
    handlePurchaseOrderNotFound(HttpServletRequest req, Exception ex) {
        return new ErrorInfo(req.getRequestURL().toString(),"" + HttpStatus.NOT_FOUND, ex);
    }

    @ResponseStatus(HttpStatus.CONFLICT)
    @ExceptionHandler(PurchaseOrderInvalidDateException.class)
    @ResponseBody
    ErrorInfo
    handlePurchaseOrderInvalidDate(HttpServletRequest req, Exception ex) {
        return new ErrorInfo(req.getRequestURL().toString(),"" + HttpStatus.CONFLICT, ex);
    }



}
