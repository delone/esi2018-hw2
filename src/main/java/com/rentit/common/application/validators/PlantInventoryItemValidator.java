package com.rentit.common.application.validators;

import com.rentit.inventory.domain.model.PlantInventoryItem;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class PlantInventoryItemValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return PlantInventoryItem.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        PlantInventoryItem item = (PlantInventoryItem) o;

    }
}
