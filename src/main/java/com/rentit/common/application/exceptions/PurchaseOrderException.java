package com.rentit.common.application.exceptions;

public class PurchaseOrderException extends Exception {

    public PurchaseOrderException(String message) {
        super(message);
    }

}
