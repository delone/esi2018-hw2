package com.rentit.inventory.application.dto;

import com.rentit.common.application.dto.BusinessPeriodDTO;
import lombok.Data;
import org.springframework.hateoas.ResourceSupport;

@Data
public class PlantReservationDTO extends ResourceSupport {
    Long _id;
    PlantInventoryItemDTO plant;
    BusinessPeriodDTO schedule;
}