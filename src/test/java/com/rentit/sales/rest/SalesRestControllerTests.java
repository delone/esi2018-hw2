package com.rentit.sales.rest;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rentit.RentitApplication;
import com.rentit.common.application.dto.BusinessPeriodDTO;
import com.rentit.inventory.application.dto.PlantInventoryEntryDTO;
import com.rentit.inventory.application.dto.PlantInventoryItemDTO;
import com.rentit.inventory.domain.repository.PlantInventoryEntryRepository;
import com.rentit.sales.application.dto.PurchaseOrderDTO;
import com.rentit.sales.domain.model.POStatus;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.text.IsEmptyString.isEmptyOrNullString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = RentitApplication.class) // Check if the name of this class is correct or not
@WebAppConfiguration
@DirtiesContext
public class SalesRestControllerTests {
    @Autowired
    PlantInventoryEntryRepository repo;

    @Autowired
    private WebApplicationContext wac;
    private MockMvc mockMvc;

    @Autowired
    ObjectMapper mapper;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    @Test
    @Sql("/plants-dataset.sql")
    public void testGetAllPlants() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/sales/plants?name=Exc&startDate=2017-04-14&endDate=2017-04-25"))
                .andExpect(status().isOk())
                .andExpect(header().string("Location", isEmptyOrNullString()))
                .andReturn();

        List<PlantInventoryEntryDTO> plants = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<List<PlantInventoryEntryDTO>>() {});

        assertThat(plants.size()).isEqualTo(3);

        PurchaseOrderDTO order = new PurchaseOrderDTO();
        order.setPlant(plants.get(1));
        order.setRentalPeriod(BusinessPeriodDTO.of(LocalDate.now(), LocalDate.now()));

        mockMvc.perform(post("/api/sales/orders").content(mapper.writeValueAsString(order)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    @Sql("/plants-dataset.sql")
    public void testPurchaseOrderIdentifierValidity() throws Exception {
        createPurchaseOrder();

        MvcResult result = mockMvc.perform(get("/api/sales/orders"))
                .andExpect(status().isOk())
                .andExpect(header().string("Location", isEmptyOrNullString()))
                .andReturn();

        List<PurchaseOrderDTO> pos = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<List<PurchaseOrderDTO>>() {});

        assertThat(pos.size()).isEqualTo(1);
        PurchaseOrderDTO purchaseOrderDTO = pos.get(0);
        assertThat(purchaseOrderDTO.get_id()).isEqualTo(1);

    }

    /*
     *
     * An OPEN/CLOSED PO must have a valid plant reservation
     * (e.g. the period of the reservation must be consistent with the PO rental period,
     * the reservation must be connected with an plant inventory item that was available before the reservation)
     *
     * */

    @Test
    @Sql("/plants-dataset.sql")
    public void testOpenOrClosedPurchaseOrderConsistency() throws Exception{

        //get available plants for given period
        MvcResult result = mockMvc.perform(get("/api/sales/plants?name=Exc&startDate=2020-04-20&endDate=2020-04-25"))
                .andExpect(status().isOk())
                .andExpect(header().string("Location", isEmptyOrNullString()))
                .andReturn();

        //retrieve list of plants entity objects from JSON
        List<PlantInventoryEntryDTO> plants = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<List<PlantInventoryEntryDTO>>() {});

        //create a PO object
        PurchaseOrderDTO order = new PurchaseOrderDTO();
        BusinessPeriodDTO businessPeriod = BusinessPeriodDTO.of(LocalDate.of(2020, 4, 20), LocalDate.of(2020, 4, 20));
        order.setPlant(plants.get(1));
        order.setRentalPeriod(businessPeriod);

        //insert PO object into DB
        MvcResult result2 = mockMvc.perform(post("/api/sales/orders").content(mapper.writeValueAsString(order)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn();

        //retrieve inserted PO from JSON in order to get its ID
        PurchaseOrderDTO poDTO = mapper.readValue(result2.getResponse().getContentAsString(), PurchaseOrderDTO.class);

        //check PO status
        assertThat(poDTO.getStatus()).isEqualTo(POStatus.PENDING);
        System.out.println("PO status is PENDING");

        //get all available plant items from DB that are relevant to given PO
        MvcResult result3 = mockMvc.perform(get("/api/sales/orders/" + poDTO.get_id() + "/plant/items"))
                .andExpect(status().isOk())
                .andExpect(header().string("Location", isEmptyOrNullString()))
                .andReturn();

        //retrieve items list from JSON, then choose the 1st one as item
        List<PlantInventoryItemDTO> items = mapper.readValue(result3.getResponse().getContentAsString(), new TypeReference<List<PlantInventoryItemDTO>>() {});
        PlantInventoryItemDTO item = items.get(0);

        //accept order regarding the chosen item object
        MvcResult result4 = mockMvc.perform(post("/api/sales/orders/"+ poDTO.get_id() +"/accept").content(mapper.writeValueAsString(item)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn();

        //retrieve PO from JSON
        poDTO = mapper.readValue(result4.getResponse().getContentAsString(), PurchaseOrderDTO.class);

        //check its status
        assertThat(poDTO.getStatus()).isEqualTo(POStatus.OPEN);
        System.out.println("PO status is OPEN");

        //check the item is reserved correctly
        assertThat(poDTO.getReservations().get(0).getPlant()).isEqualTo(item);
        System.out.println("Item reserved according to PO");

        //check reservation period
        assertThat(poDTO.getReservations().get(0).getSchedule()).isEqualTo(businessPeriod);
        System.out.println("Business period of Reservation is correct according to PO");
    }

    @Test
    @Sql("/plants-dataset.sql")
    public void testPurchaseOrderValidity() throws Exception {
        createPurchaseOrder();

        MvcResult result = mockMvc.perform(get("/api/sales/orders"))
                .andExpect(status().isOk())
                .andExpect(header().string("Location", isEmptyOrNullString()))
                .andReturn();

        List<PurchaseOrderDTO> pos = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<List<PurchaseOrderDTO>>() {
        });

        assertThat(pos.size()).isEqualTo(1);
        PurchaseOrderDTO purchaseOrderDTO = pos.get(0);
        assertThat(purchaseOrderDTO.getPlant().get_id()).isEqualTo(2);
        assertThat(purchaseOrderDTO.getRentalPeriod().getStartDate()).isLessThan(purchaseOrderDTO.getRentalPeriod().getEndDate());
        assertThat(purchaseOrderDTO.getRentalPeriod().getStartDate()).isGreaterThanOrEqualTo(LocalDate.now());
        assertThat(purchaseOrderDTO.getRentalPeriod().getEndDate()).isGreaterThan(LocalDate.now());
        assertThat(purchaseOrderDTO.getRentalPeriod().getEndDate()).isNotEqualTo(null);
        assertThat(purchaseOrderDTO.getRentalPeriod().getStartDate()).isNotEqualTo(null);
        assertThat(purchaseOrderDTO.getTotal()).isEqualTo(new BigDecimal("2200.00"));
    }

    //An OPEN/CLOSED PO must have a valid total cost (e.g. positive value)
    @Test
    @Sql("/plants-dataset.sql")
    public void testCheckValidTotalCost() throws Exception {
        //get available plants for given period
        MvcResult result = mockMvc.perform(get("/api/sales/plants?name=Exc&startDate=2020-04-20&endDate=2020-04-25"))
                .andExpect(status().isOk())
                .andExpect(header().string("Location", isEmptyOrNullString()))
                .andReturn();

        //retrieve list of plants entity objects from JSON
        List<PlantInventoryEntryDTO> plants = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<List<PlantInventoryEntryDTO>>() {
        });

        //create a PO object
        PurchaseOrderDTO order = new PurchaseOrderDTO();
        BusinessPeriodDTO businessPeriod = BusinessPeriodDTO.of(LocalDate.of(2020, 4, 20), LocalDate.of(2020, 4, 20));
        order.setPlant(plants.get(1));
        order.setRentalPeriod(businessPeriod);

        //insert PO object into DB
        MvcResult result2 = mockMvc.perform(post("/api/sales/orders").content(mapper.writeValueAsString(order)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn();

        //retrieve inserted PO from JSON in order to get its ID
        PurchaseOrderDTO poDTO = mapper.readValue(result2.getResponse().getContentAsString(), PurchaseOrderDTO.class);

        //check PO status
        assertThat(poDTO.getStatus()).isEqualTo(POStatus.PENDING);
        System.out.println("PO status is PENDING");

        //get all available plant items from DB that are relevant to given PO
        MvcResult result3 = mockMvc.perform(get("/api/sales/orders/" + poDTO.get_id() + "/plant/items"))
                .andExpect(status().isOk())
                .andExpect(header().string("Location", isEmptyOrNullString()))
                .andReturn();

        //retrieve items list from JSON, then choose the 1st one as item
        List<PlantInventoryItemDTO> items = mapper.readValue(result3.getResponse().getContentAsString(), new TypeReference<List<PlantInventoryItemDTO>>() {
        });
        PlantInventoryItemDTO item = items.get(0);

        //accept order regarding the chosen item object
        MvcResult result4 = mockMvc.perform(post("/api/sales/orders/" + poDTO.get_id() + "/accept").content(mapper.writeValueAsString(item)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn();

        //retrieve PO from JSON
        poDTO = mapper.readValue(result4.getResponse().getContentAsString(), PurchaseOrderDTO.class);

        //check its status
        assertThat(poDTO.getStatus()).isEqualTo(POStatus.OPEN);
        System.out.println("PO status is OPEN");

        //check the total price of the PO
        assertThat(poDTO.getTotal()).isEqualTo(calculateTotalPrice(item.getPlantInfo(), businessPeriod));
        System.out.println("PO total price check");
    }

    private BigDecimal calculateTotalPrice(PlantInventoryEntryDTO entry, BusinessPeriodDTO period) {
        LocalDate startDate = period.getStartDate();
        LocalDate endDate = period.getEndDate();
        BigDecimal price = entry.getPrice();

        BigDecimal days = new BigDecimal(ChronoUnit.DAYS.between(startDate, endDate));

        return days.multiply(price);
    }

    private void createPurchaseOrder() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/sales/plants?name=Exc&startDate=2018-04-14&endDate=2018-04-25"))
                .andExpect(status().isOk())
                .andExpect(header().string("Location", isEmptyOrNullString()))
                .andReturn();
        List<PlantInventoryEntryDTO> plants = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<List<PlantInventoryEntryDTO>>() {
        });
        PurchaseOrderDTO order = new PurchaseOrderDTO();
        order.setPlant(plants.get(1));
        order.setRentalPeriod(BusinessPeriodDTO.of(LocalDate.of(2018, 04, 14), LocalDate.of(2018, 04, 25)));
        mockMvc.perform(post("/api/sales/orders").content(mapper.writeValueAsString(order)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

}
