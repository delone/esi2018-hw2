package com.rentit.common.application.exceptions;

public class PlantInventoryItemException extends Exception {
    public PlantInventoryItemException(String message) {
        super(message);
    }
}
