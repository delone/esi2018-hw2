package com.rentit.common.application.exceptions;

public class PurchaseOrderNotFoundException extends PurchaseOrderException {
    public PurchaseOrderNotFoundException() {
            super(PurchaseOrderNotFoundException.class.getName() + " : " + "Purchase order not found!");
    }
}
