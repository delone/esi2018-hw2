package com.rentit.inventory.application.services;

import com.rentit.common.application.dto.BusinessPeriodDTO;
import com.rentit.inventory.application.dto.PlantInventoryItemDTO;
import com.rentit.inventory.application.dto.PlantReservationDTO;
import com.rentit.inventory.domain.model.PlantInventoryItem;
import com.rentit.inventory.domain.model.PlantReservation;
import com.rentit.inventory.rest.InventoryRestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PlantReservationAssembler extends ResourceAssemblerSupport<PlantReservation, PlantReservationDTO> {

    @Autowired
    PlantInventoryItemAssembler plantInventoryItemAssembler;

    public PlantReservationAssembler() { super(InventoryRestController.class, PlantReservationDTO.class); }

    @Override
    public PlantReservationDTO toResource(PlantReservation plantReservation) {
        PlantReservationDTO dto = createResourceWithId(plantReservation.getId(), plantReservation);
        dto.set_id(plantReservation.getId());
        dto.setPlant(plantInventoryItemAssembler.toResource(plantReservation.getPlant()));
        dto.setSchedule(BusinessPeriodDTO.of(plantReservation.getSchedule().getStartDate(), plantReservation.getSchedule().getEndDate()));

        return dto;
    }

    public List<PlantReservationDTO> toResources(List<PlantReservation> plantReservations) {

        List<PlantReservationDTO> dtoList = new ArrayList<>();
        plantReservations.forEach(po -> dtoList.add(toResource(po)));
        return dtoList;
    }

}
