package com.rentit.common.application.validators;

import com.rentit.inventory.domain.model.PlantInventoryEntry;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class PlantInventoryEntryValidator implements Validator {

    @Override
    public boolean supports(Class<?> aClass) {
        return PlantInventoryEntry.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        PlantInventoryEntry entry = (PlantInventoryEntry) o;

//        if (entry.getId() < 2) {
//            errors.rejectValue("id", "plant id cannot be null");
//        }
    }
}
